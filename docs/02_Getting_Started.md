# Getting Started

To illustrated CIM, Data Models, and the differences, we are going to load some [data](data_fakenet.log) in to Splunk.

This data set will be onboarded, and will provide the base for the rest of this exercise.

*Assumption: you have access to a local Splunk server on your system (i.e. http://localhost:8000).  If you do not have this, please proceed to grab a [free download](https://www.splunk.com/download)*

## Step 1 - Install the Common Information model App

Before starting this lab, please ensure you have the [Splunk Common Information Model](https://splunkbase.splunk.com/app/1621/) app installed.

## Step 2 - Creating our Technology Add-On

First step is creating an application to house all our configurations.  This will help centralize out setup, and simplify future deployments.

For simplicity, a shell of an a TA can be downloaded: [TA-fakenet](TA-fakenet.spl).  More details of this TA can be viewed under the [TA-fakenet folder](TA-fakenet/).

## Step 3 - Load the data

Navigate to `Settings > Add Data` and load the `data_fakenet.log` file.

--> COMPLETE THIS SECTION

## Santity Check

Ensure you have completed at least the following:
* Installed the `Common Information Model`
* Installed the placeholder app `TA-takenet`
* loaded `data_fakenet.log` into an index

### Extra Content
Scripts to generate sample data
```bash
for h in $(seq -f "%02g" 0 23); do
  for m in $(seq -f "%02g" 0 59); do
    for s in $(seq -f "%02g" 0 59); do
      echo "$h:$m:$s if=int of=ext go=yes who=172.24.2.$(( ( RANDOM % 254 )  + 1 )) where=10.0.0.$(( ( RANDOM % 10 )  + 1 ))" src_port=$(( ( RANDOM % 3000 ) + 1024)) dst_port=80 protocol=tcp bi=$(( RANDOM % 3000 )) bo=$(( RANDOM % 3000 )) 
    done
  done
done
```

[Next - Am I CIM Compliant?](03_Am_I_CIM_Compliant.md)