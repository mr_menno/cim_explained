# Am I CIM Compliant?

Possibly - but how do I now?

The key criteria to determining CIM compliance is:
1. Do I have all the fields needed / available?
2. Do I have an eventtype for the data?

   _You do not need an event type, but it makes tagging easier as you can tag event types, as compared to tagging individual fields._
   
3. Does the eventtype have the right tags?