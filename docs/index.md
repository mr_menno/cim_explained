# CIM Explained.

This tutorial guide is to walk you through the common information model, and hopefully explain the relationship between data models, common information model, and how they both are used.

## Agenda

1. [Introductions](01_Intro.md)
2. [Getting Started](02_Getting_Started.md)
3. [Am I CIM Complaint?](03_Am_I_CIM_Compliant.md)
4. [What's a data model?](04_Whats_a_Data_Model.md)
5. [Getting CIM Compliant.md](05_Getting_CIM_Compliant.md)
6. [Working the models](06_Working_the_models.md)

## Intro

Splunk uses schema at read to extract and define all fields at search time.  This produces amazing fast results, but does run into a few challenges.  There are multiple use cases where you want to have multiple data sets all adhering to a common field name convention.  With Splunk's amazing community, a multitude of technology add-ons have been created, and they someone followed their own naming conventions.

## What's the Common Information Model (CIM)

The Splunk Common Information Model (CIM) is a shared semantic model focused on extracting value from data. The CIM is implemented as an add-on that contains a collection of data models, documentation, and tools that support the consistent, normalized treatment of data for maximum efficiency at search time.

## Why the CIM exists

The CIM helps you to normalize your data to match a common standard, using the same field names and event tags for equivalent events from different sources or vendors. The CIM acts as a search-time schema ("schema-on-the-fly") to allow you to define relationships in the event data while leaving the raw machine data intact. 

## CIM and Data Models

Common Information Model is a contract, it is the field definitions which should exists for a specific purpose.  Example: any *Network Traffic* data should contain a field called *action* and a field called *src_ip*.  If the data does not contain this, it is not compliant with the Common Information Model.

Data Models are structured data sets within Splunk.  The data sets look for data (generally speaking by tags), and have defined fields which map to fields which should exist in the data.

In the example of the `Network Traffic` data model, it looks for data with `tag=network tag=communicate`, and expects to see fields such as `src_ip` and `action`.

_For more details visit: [Splunk CIM](https://docs.splunk.com/Documentation/CIM/4.13.0/User/Overview)_

[Next - Getting Started](02_Getting_Started.md)
